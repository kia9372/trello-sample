import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GenericModel } from '../models';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' })
};

@Injectable({
    providedIn: 'root'
})
export class GenericServicService<T> {
    private headers: HttpHeaders;

    constructor(public httpClient: HttpClient) { }

    // Create a Geberic Service with Generic Model for Create Object in Database
    public Create(item: T, url: string): Observable<GenericModel<T>> {
        return this.httpClient.post<GenericModel<T>>(`${url}`, item);
    }

    // Create a Geberic Service with Generic Model for Get All Object 
    public GetListItem(url: string): Observable<GenericModel<T[]>> {
        return this.httpClient.get<GenericModel<T[]>>(`${url}`);
    }

    // Create a Geberic Service with Generic Model for Get Object From Database with Id  
    public GetItemById(item: number, url: string): Observable<GenericModel<T>> {
        return this.httpClient.post<GenericModel<T>>(`${url}`, item);
    }

    // Create a Geberic Service with Generic Model for Update Object  
    public Update(item: T, url: string): Observable<GenericModel<T>> {
        return this.httpClient.put<GenericModel<T>>(`${url}`, item);
    }

    // Create a Geberic Service with Generic Model for Delete Object  
    public Delete(item: number, url: string): Observable<GenericModel<T>> {
        return this.httpClient.post<GenericModel<T>>(`${url}`, item);
    }
}