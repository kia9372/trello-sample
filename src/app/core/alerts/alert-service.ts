import { Injectable } from "@angular/core";
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})

export class AlertService {
    constructor(private toastr: ToastrService) { }

    showToast(success: boolean, message: string) {
        if (success) {
            this.toastr.success(message);
        } else {
            this.toastr.error(message);
        }
    }
}