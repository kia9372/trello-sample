export interface GenericModel<T> {
    data:T;
    isSuccess:boolean;
    statusCode:number;
    message:string;
}