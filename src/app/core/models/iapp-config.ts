import { InjectionToken } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

export let APP_CONFIG = new InjectionToken<string>('app.config');
export interface IAppConfig {
    apiEndpoint: string;
    headersOptions: HttpHeaders;
}

export const AppConfig: IAppConfig = {
    apiEndpoint: 'https://localhost:44354/',
    headersOptions : new HttpHeaders({ 'Content-Type': 'application/json' }),
};
