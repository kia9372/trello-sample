import { Component, OnInit, AfterViewInit, Injectable, Inject } from '@angular/core';
import { trigger } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared/services/shared-services';
import { TeamModel } from '../../models/team-model';
import { BoardModel } from '../../models/board-model';
import { APP_CONFIG, IAppConfig } from 'src/app/core/models/iapp-config';
import { ToastrService } from 'ngx-toastr';
import { AlertService } from 'src/app/core/alerts/alert-service';

@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.css']
})
export class TopHeaderComponent implements OnInit, AfterViewInit {


  show = false;
  modal: any;
  trigger: any;
  closeButton: any;
  team = false;
  busines = false;
  board = false;
  addTeam: FormGroup;
  addBoard: FormGroup;
  addBusines: FormGroup;
  teamModel: TeamModel;
  boardModel: BoardModel;

  constructor(
    private formBuilder: FormBuilder
    , private sharedSerice: SharedService
    , private toastr: ToastrService
    , private alert: AlertService
    , @Inject(APP_CONFIG) private appConfig: IAppConfig
  ) { }

  ngOnInit() {
    this.initlaForm();
  }

  ngAfterViewInit(): void {
    this.modal = document.querySelector(".modal");
    this.trigger = document.querySelector(".trigger");
    this.closeButton = document.querySelector(".close-button");
  }

  showCreateMenu() {
    this.show = !this.show;
  }

  initlaForm(): void {
    this.initialAddBoardForm();
    this.initialAddBusinesForm();
    this.initialAddTeamForm();
  }

  initialAddBoardForm() {
    this.addBoard = this.formBuilder.group({
      BoardName: ['', Validators.compose([Validators.required])]
    })
  }

  initialAddTeamForm() {
    this.addTeam = this.formBuilder.group({
      TeamName: ['', Validators.compose([Validators.required])]
    })
  }

  initialAddBusinesForm() {
    this.addBusines = this.formBuilder.group({
      BusinesName: ['', Validators.compose([Validators.required])]
    })
  }

  AddTeam(): void {
    const url = 'Team/AddTeam';
    this.AddValue(this.addTeam, this.appConfig.apiEndpoint + url)
  }

  AddBoard(): void {
    const url = 'Board/AddBaord';
    this.AddValue(this.addBoard, this.appConfig.apiEndpoint + url)
  }

  AddBusines(): void {
    const url = 'Team/AddTeam';
    this.AddValue(this.addBusines, this.appConfig.apiEndpoint + url)
  }


  toggleModal(name: string) {
    console.log(name)
    if (name === 'Board') {
      this.board = true;
      this.team = false
      this.busines = false;
    } else if (name === 'Team') {
      this.team = true;
      this.board = false;
      this.busines = false;
    } else if (name === 'Buasines') {
      this.busines = true;
      this.board = false;
      this.team = false;
    }
    this.modal.classList.toggle("show-modal");
  }

  windowOnClick(event) {
    if (event.target === this.modal) {
      this.toggleModal("Board");
    }
  }


  AddValue(form: FormGroup, url: string): void {
    console.log(form.value)
    this.sharedSerice.Create(form.value, url).subscribe(data => {
      this.alert.showToast(data.isSuccess, data.message)
    })
  }

}
