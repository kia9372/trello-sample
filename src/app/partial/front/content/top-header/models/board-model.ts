export interface BoardModel {
    id: number;
    boardName: string;
}