import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';
import { GenericServicService } from 'src/app/core/services';

@Injectable({
    providedIn: 'root'
})

export class SharedService extends GenericServicService<any> {


}