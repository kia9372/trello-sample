import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { moveItemInArray, CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WorkService } from '../services';
import { IAppConfig, APP_CONFIG } from 'src/app/core/models/iapp-config';
import { card, work, CARD_CONFIG, ICardconfig } from '../models';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  cards: card[] = [];
  work: work[] = [];
  addWorkFormGroup: FormGroup;

  showAddCard = false;

  constructor(
    private formBuilder: FormBuilder,
    private workService: WorkService,
    @Inject(APP_CONFIG) private appConfig: IAppConfig,
    @Inject(CARD_CONFIG) private cardConfig: ICardconfig) {
    this.work.push({
      id: 1,
      workName: 'Programming',
      boardId: 1
    })
    this.cards.push({
      id: 1,
      name: 'Daily Work',
      works: this.work
    })
  }

  ngOnInit(): void {
    this.InitialAddWorkForm();
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log(event.container.data)
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  show() {
    this.showAddCard = !this.showAddCard;
  }

  InitialAddWorkForm(): void {
    this.addWorkFormGroup = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])]
    })
  }

  addWork(): void {
    let work = {} as work;
    work.id = 1;
    work.workName = this.addWorkFormGroup.get('name').value;
    work.boardId = 1;
    this.workService.Create(work, this.appConfig.apiEndpoint + this.cardConfig.addRoute).subscribe(data => {
      if (data.isSuccess) {
        this.work.push(work);
      } else {
        console.log(data.message)
      }
    })
    this.showAddCard = false;
    this.clearForm();
  }

  clearForm() {
    this.addWorkFormGroup.reset();
  }
}
