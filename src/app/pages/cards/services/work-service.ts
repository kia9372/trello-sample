

import { GenericServicService } from 'src/app/core/services';
import { Injectable } from '@angular/core';
import { work } from '../models';

@Injectable({
    providedIn: 'root'
})

export class WorkService extends GenericServicService<work>{

}