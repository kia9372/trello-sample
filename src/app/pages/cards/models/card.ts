import { work } from './work';

export interface card {
    id: number;
    name: string;
    works: work[];
}