import { InjectionToken } from '@angular/core';

export let CARD_CONFIG = new InjectionToken<string>('card.config');

export interface ICardconfig {
    addRoute: string;
}

export const CardConfig: ICardconfig = {
    addRoute: 'DoWork/AddWork'
}