export interface AddWork {
    id: number;
    name: string;
    boardId: number;
}